# Artists Microservice

This is a demo app to provide CRUD operations to a MongoDB database that contains music artists using REST API.

Currently deployed on Bluemix with public ip **169.44.7.47**

### Currently it supports the following REST API functions
- GET
- POST
- DELETE
- PUT

### Table of Contents
  - Preparation
  - Run Locally
  - Run using Docker
  - Deploy to Bluemix

### Preparation
* [Meteor] - Javascript framework that uses Node.js + MongoDB
* [Docker] - Container service
* Any text editor that can edit files
* Way to call REST API (curl, Postman...)

### Run Locally
This microservice uses Meteor so install Meteor first...
Follow the following instructions to get the microservice up and running.
```sh
$ git clone <this repo link>
$ cd microservice-artists
$ meteor run
```
The Meteor application will be running on http://localhost:3000, use any REST API client to call the following functions:

##### GET

  - lists all artists in the database
  - http://`<url>`/api/artists
  - returns a JSON object with all the artists in the database

##### POST

  - inserts an artist into the database
  - data section should be a JSON formatted string (remember to set the data type to be JSON)

      ```json
      {
    	"name": "Ryan", 
    	"street": "HK", 
    	"pobox": "HK", 
    	"city": "HK", 
    	"state_province": "HK", 
    	"country": "HK", 
    	"zip": "123456", 
    	"email": "test@gmail.com", 
    	"instruments": 
    		[
    			"guitar", 
    			"piano",
    			"violin"
    		]
      }
      ```
      
  - http://`<url>`/api/artists
  - returns a unique mongodb object id in JSON

##### PUT

  - updates an artist inside the database
  - remember to set the data type to be JSON
  - http://`<url>`/api/artists/`<mongodb_object_id>`
  - returns the updated artist in JSON

##### DELETE

  - removes an artist inside the database
  - http://`<url>`/api/artists/`<mongodb_object_id>`
  - returns the deleted mongo object id in JSON

### Run using Docker

You need Docker installed first:

**Building the image**

Since a *Dockerfile* is provided, building it is very easy. Goto the directory where the *Dockerfile* is contained
```sh
$ docker build -t <yourname>/<app> .
```

**Running the image**

```sh
$ docker run -p 8080:80 -e MONGO_URL=mongodb://test:test@ds059115.mongolab.com:59115/meteordbtest -e ROOT_URL=http://127.0.0.1 <yourname>/<app>
```
When you see **=> Starting meteor app on port:80** it means the microservice has started successfully. Use any REST API client to access the database @ **http://localhost:8080** since I forwarded port *80* in the Docker container to *8080* on the host machine.

### Deploy to Bluemix

This part is kind of time consuming... You have to do the following preparations...
- Install **cf** CLI tool for BLuemix
- Install **ic** plugin for cf CLI
- Create a Space in Bluemix
- use the **cf login** command to login
- use the **cf ic login** command to login

##### Renaming the Docker image to push to Bluemix

```sh
$ cf ic namespace get #use this command to get your Bluemix namespace
$ docker tag <yourname>/<app> registry.ng.bluemix.net/<namespace>/<image_name>
$ docker push registry.ng.bluemix.net/<namespace>/<image_name>
```

After these steps the Docker image should be in your Bluemix account...
Next follow these steps on the Bluemix website https://console.ng.bluemix.net/

1. Login, goto your Dashboard and select the correct Space
2. Click **Start Containers**
3. Select the correct container image
4. Type in a **Container Name**
5. Under **Public IP address**, select **Request and Bind Public IP**
6. Click **Advanced Options**
7. Click **Add a new environment variable**
8. in the **Enter key** box type in: **MONGO_URL**
9. in the **Enter value** box type in: **mongodb://test:test@ds059115.mongolab.com:59115/meteordbtest**
10. Click **Add a new environment variable** again
11. in the **Enter key** box type in: **ROOT_URL**
12. in the **Enter value** box type in: **http://127.0.0.1**
13. Everything else leave as default
14. Click **Create**
15. Wait and everything should just work magically
16. Back in your Space, check out the newly created container. There should be a **Public IP**
17. Use the Public IP to access the REST API

> Warning: In order to connect to the external MongoDB properly, I have set the container to start 10 seconds after it boots. It is a bug within Bluemix Containers that it needs some time for the network settings to initialize

[//]: # 
   [Meteor]: <https://www.meteor.com/>
   [Docker]: <https://www.docker.com/>