Artists = new Mongo.Collection("artists");

if (Meteor.isServer) {
  Meteor.startup(function () {
    if(Artists.find({}).count == 0)
    {
      Artists.insert({
        name: "Ryan", 
        street: "HK", 
        pobox: "HK", 
        city: "HK", 
        state_province: "HK", 
        country: "HK", 
        zip: "123456", 
        email: "test@test.com", 
        instruments: 
          [
            "guitar", 
            "piano"
          ]
      });
    }
  });

  var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
  });

  Api.addCollection(Artists);
}
